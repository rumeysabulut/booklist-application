# Turkcell BookList Case - Gncytnk 20

This project lists books from a txt file and does searching and filtering in the list.
The project is initialized using https://start.spring.io It is a spring boot application which uses Maven 
as its project management tool.
Thanks https://github.com/Omair31/BooksCSV/tree/master/BooksCSV for book list.

## How to build and run the project?
You should give the file path as command line argument. If you want to add it from ide, you can add it in 
'Edit Configurations->Program arguments' tab. Then, hit the run button. If you run program in terminal, run command 
`./mvnw spring-boot:run -Dspring-boot.run.arguments=filePath` in the project's root directory.

You can create an executable jar file using `./mvnw clean install` and run that file with command
`java -jar target/turkcell-case-0.0.1-SNAPSHOT.jar filePath` in the project's root directory. turkcell-case-0.0.1-SNAPSHOT.jar 
is the name of executable jar file.

### Example Requests
If programs runs in port 8080:
- http://localhost:8080/booklist

- http://localhost:8080/booklist/authors

- http://localhost:8080/booklist/publishers
  
- http://localhost:8080/booklist/years

- http://localhost:8080/booklist/searchWithCriteria?bookName=rain
  
- http://localhost:8080/booklist/searchWithCriteria?author=son
  
- http://localhost:8080/booklist/searchWithCriteria?publisher=scho
  
- http://localhost:8080/booklist/searchWithCriteria?year=200
  
- http://localhost:8080/booklist/searchWithCriteria?year=1998
  
- http://localhost:8080/booklist/searchWithAnyText?searchText=arry
  
- http://localhost:8080/booklist/searchWithAllFields?bookName=pot&author=row&publisher=scho&year=200
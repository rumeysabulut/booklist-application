package com.rumeysabulut.turkcellcase;

import java.util.*;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@RestController
public class BookListController {
    private static List<Book> bookList = initBookList(TurkcellCaseApplication.defaultPath != null ? TurkcellCaseApplication.defaultPath : "data/BookList.txt");

    public BookListController(String path) {
        BookListController.bookList = initBookList(path);
    }

    // used in the first run
    public BookListController() {
        String defaultPath = TurkcellCaseApplication.defaultPath;
        if (BookListController.bookList == null) {
            initBookList(defaultPath);
        }
    }

    private static List<Book> initBookList(String path) {
        File file = new File(path);
        List<Book> bookList = null;
        try {
            bookList = readFile(file, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bookList;
    }

    public static List<Book> readFile(File file, Charset encoding) throws IOException {
        FileInputStream fileStream = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fileStream, encoding));
        List<Book> bookList = new ArrayList<>();

        String line;
        String[] assetClasses;
        long id;
        String name, author, publisher;
        int year;
        Book book;
        while ((line = br.readLine()) != null) {
            assetClasses = line.split(";");
            id = Long.parseLong(assetClasses[0]);
            name = assetClasses[1];
            author = assetClasses[2];
            publisher = assetClasses[3];
            year = Integer.parseInt(assetClasses[4]);

            book = new Book(id, name, author, publisher, year);
            bookList.add(book);
        }
        br.close();
        return bookList;
    }

    @GetMapping("/booklist") // returns all books in txt file
    public List<Book> listBooks() {
        return bookList;
    }

    @GetMapping("/booklist/authors") // returns books grouped by their authors
    public LinkedHashMap<String, List<Book>> listOnlyBookAuthors() {
        LinkedHashMap<String, List<Book>> booksMappedByAuthors = new LinkedHashMap<>();
        bookList.forEach(book -> {
            String authorName = book.getAuthor();
            List<Book> publishersBooks = booksMappedByAuthors.getOrDefault(authorName, new ArrayList<>());
            publishersBooks.add(book);
            booksMappedByAuthors.put(authorName, publishersBooks);
        });
        return booksMappedByAuthors;
    }

    @GetMapping("/booklist/publishers") // returns books grouped by their publishers
    public LinkedHashMap<String, List<Book>> listOnlyBookPublishers() {
        LinkedHashMap<String, List<Book>> booksMappedByPublishers = new LinkedHashMap<>();
        bookList.forEach(book -> {
            String publisherName = book.getPublisher();
            List<Book> publishersBooks = booksMappedByPublishers.getOrDefault(publisherName, new ArrayList<>());
            publishersBooks.add(book);
            booksMappedByPublishers.put(publisherName, publishersBooks);
        });
        return booksMappedByPublishers;
    }

    @GetMapping("/booklist/years") // returns books grouped by their publishing years
    public LinkedHashMap<Integer, List<Book>> listOnlyBookPublishingYears() {
        LinkedHashMap<Integer, List<Book>> booksMappedByPublishingYears= new LinkedHashMap<>();
        bookList.forEach(book -> {
            Integer year = book.getYear();
            List<Book> bookPublishingYears = booksMappedByPublishingYears.getOrDefault(year, new ArrayList<>());
            bookPublishingYears.add(book);
            booksMappedByPublishingYears.put(year, bookPublishingYears);
        });
        return booksMappedByPublishingYears;
    }

    @GetMapping("/booklist/searchWithCriteria") // searching a book with its name or author or publisher or year
    public List<Book> searchWithCriteria(
        @RequestParam(value = "bookName", required = false) String bookName,
        @RequestParam(value = "author", required = false) String author,
        @RequestParam(value = "publisher", required = false) String publisher,
        @RequestParam(value = "year", required = false) String year
    ) {
        List<Book> filteredBooks = new ArrayList<>();

        bookList.forEach(book -> {
            if (
                (bookName != null && book.getName().toLowerCase().contains(bookName.toLowerCase())) ||
                (author != null && book.getAuthor().toLowerCase().contains(author.toLowerCase())) ||
                (publisher != null && book.getPublisher().toLowerCase().contains(publisher.toLowerCase())) ||
                (year != null && Integer.toString(book.getYear()).contains(year))
            ) {
                filteredBooks.add(book);
            }
        });
        return filteredBooks;
    }

    @GetMapping("/booklist/searchWithAnyText") // search a book with a random text. Look for arry, and returns both Harry Potter books and author Garry Wills books
    public List<Book> searchWithAnyText(@RequestParam(value = "searchText") String searchText){
        List<Book> filteredBooks = new ArrayList<>();
        bookList.forEach(book -> {
            if (searchText != null && (
                book.getName().toLowerCase().contains(searchText.toLowerCase()) ||
                book.getAuthor().toLowerCase().contains(searchText.toLowerCase()) ||
                book.getPublisher().toLowerCase().contains(searchText.toLowerCase())
            )) {
                filteredBooks.add(book);
            }
        });
        return filteredBooks;
    }

    @GetMapping("/booklist/searchWithAllFields") // search a book with its name and author and publisher and year
    public List<Book> searchWithAllFields(
        @RequestParam(value = "bookName") String bookName,
        @RequestParam(value = "author") String author,
        @RequestParam(value = "publisher") String publisher,
        @RequestParam(value = "year") String year
    ){
        List<Book> filteredBooks = new ArrayList<>();
        bookList.forEach(book -> {
            if (
                book.getName().toLowerCase().contains(bookName.toLowerCase()) &&
                book.getAuthor().toLowerCase().contains(author.toLowerCase()) &&
                book.getPublisher().toLowerCase().contains(publisher.toLowerCase()) &&
                Integer.toString(book.getYear()).contains(year)
            ) {
                filteredBooks.add(book);
            }
        });
        return filteredBooks;
    }
}
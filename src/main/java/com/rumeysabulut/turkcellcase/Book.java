package com.rumeysabulut.turkcellcase;

public class Book {

    private long id;
    private String name;
    private String author;
    private String publisher;
    private int year;

    public Book(long id, String name, String author, String publisher, int year) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
    }

    @Override
    public boolean equals(Object obj) {
        Book comparedOne = (Book) obj;
        return this.id == comparedOne.getId() && this.name.equals(comparedOne.getName())
                && this.author.equals(comparedOne.getAuthor()) && this.year == comparedOne.year;
    }

    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getAuthor() {
        return author;
    }
    public String getPublisher() {
        return publisher;
    }
    public int getYear() {
        return year;
    }
}

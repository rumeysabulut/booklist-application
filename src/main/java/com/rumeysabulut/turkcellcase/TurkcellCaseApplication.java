package com.rumeysabulut.turkcellcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TurkcellCaseApplication {
	static String defaultPath;

	public static void main(String[] args) {
		defaultPath = args[0];
		SpringApplication.run(TurkcellCaseApplication.class, args);
	}

}

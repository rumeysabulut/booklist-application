package com.rumeysabulut.turkcellcase;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BookListControllerTests {
    private static BookListController controller;

    private Book bookWithId1 = new Book(1, "Harry Potter and the Half-Blood Prince (Harry Potter  6)",
            "J.K. Rowling/Mary GrandPré", "Scholastic Inc.", 2006);

    private Book bookWithId21 = new Book(21, "A Short History of Nearly Everything", "Bill Bryson",
            "Broadway Books",2004);

    private Book bookWithId394 = new Book(394, "Lincoln at Gettysburg: The Words That Remade America",
            "Garry Wills", "Simon & Schuster",1993);

    @BeforeAll
    static void initBookList() {
        String dataPath = "data/TestBookList.txt";
        controller = new BookListController(dataPath);
    }
    @Test
    public void testGetBookListSuccess() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + 8080 + "/booklist";
        URI uri = new URI(baseUrl);

        ResponseEntity<List> result = restTemplate.getForEntity(uri, List.class);

        // Verify request succeed
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void testGetBookAuthorsSuccess() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + 8080 + "/booklist/authors";
        URI uri = new URI(baseUrl);

        ResponseEntity<LinkedHashMap> result = restTemplate.getForEntity(uri, LinkedHashMap.class);

        // Verify request succeed
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void testGetBookPublishersSuccess() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + 8080 + "/booklist/publishers";
        URI uri = new URI(baseUrl);

        ResponseEntity<LinkedHashMap> result = restTemplate.getForEntity(uri, LinkedHashMap.class);

        // Verify request succeed
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void testGetBookPublishingYearsSuccess() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:" + 8080 + "/booklist/years";
        URI uri = new URI(baseUrl);

        ResponseEntity<LinkedHashMap> result = restTemplate.getForEntity(uri, LinkedHashMap.class);

        // Verify request succeed
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void searchWithCriteria_nameEqualsthing_returnsListWithOnly_bookWithId21() {
        List<Book> result = controller.searchWithCriteria("thing", null, null, null);
        assertEquals(result.size(), 1);
        assertEquals(result.get(0), bookWithId21);
    }

    @Test
    void searchWithCriteria_authorEqualsSon_returnsListWithOnly_bookWithId21() {
        List<Book> result = controller.searchWithCriteria(null, "son", null, null);
        assertEquals(result.size(), 1);
        assertEquals(result.get(0), bookWithId21);
    }

    @Test
    void searchWithCriteria_publisherEqualsWay_returnsListWithOnly_bookWithId21() {
        List<Book> result = controller.searchWithCriteria(null, null, "way", null);
        assertEquals(result.size(), 1);
        assertEquals(result.get(0), bookWithId21);
    }

    @Test
    void searchWithCriteria_yearEquals06_returnsListWithOnly_bookWithId1() {
        List<Book> result = controller.searchWithCriteria(null, null, null, "06");
        assertEquals(result.size(), 1);
        assertEquals(result.get(0), bookWithId1);
    }

    @Test
    void searchWithAnyText_searchTextEqualsArry_returnsListOfBooks_Id1andId394() {
        List<Book> result = controller.searchWithAnyText("arry");
        assertEquals(result.size(), 2); // Book Harry Potter and Author Garry Wills
        assertEquals(result.get(0), bookWithId1);
        assertEquals(result.get(1), bookWithId394);
    }

    @Test
    void searchWithAllFields_nameEqualsHarry_authorEqualsRowl_publisherEqualsSchol_yearEquals06_returnsListOfBookOnlyId1() {
        List<Book> result = controller.searchWithAllFields("harry", "rowl", "schol", "06");
        assertEquals(result.size(), 1); // Book Harry Potter and Author Garry Wills
        assertEquals(result.get(0), bookWithId1);
    }
}